# Lit-HTML Barebone Project

A Lit-HTML v1 barebone project supporting custom elements

`To run project`

```bash
npm install -g live-server
cd <project directory>
live-server
```

Documentation: <https://lit.dev/docs/v1/lit-html/introduction/>
