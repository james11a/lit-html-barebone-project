/** Core imports */
import { html, render } from './lit-html/lit-html.js';

export class LitBase extends HTMLElement {
    constructor() {
        super();
        this.__need_render = false;
        this.constructor.observedAttributes.forEach(f => {
            Object.defineProperty(this, f, {
                get: () => this.getAttribute(f),
                set: (val) => this.setAttribute(f, val)
            });
        });

        this.__props = {};
        this.constructor.observedProperties.forEach(f => {
            Object.defineProperty(this, f, {
                get: () => this.__props[f],
                set: (val) => {
                    this.__props[f] = val;
                    this.reRender();
                }
            });
        });
        this.shadow = this.attachShadow({ mode: "open" });
    }

    static get observedAttributes() {
        return [];
    }

    static get observedProperties() {
        return [];
    }

    /** Function invoked when a component is added to the document's DOM */
    connectedCallback() {
        this.reRender();
    }

    /** Function invoked when a component is removed from the document's DOM */
    disconnectedCallback() {
    }

    /** Function invoked when one of the element’s observedAttributes changes */
    attributeChangedCallback(prop, oldVal, newVal) {
        this.reRender();
    }

    /** Render function returning the HTML to be displayed */
    render() {
        return html`Render function not defined`;
    }

    reRender() {
        if (!this.__need_render) {
            this.__need_render = true;
            Promise.resolve().then(() => {
                this.__need_render = false;
                let template = this.render();
                render(template, this.shadow, { eventContext: this });

                if (this.afterRender) {
                    Promise.resolve().then(() => this.afterRender());
                }
            });
        }
    }

    /** Function to dispatch a custom event to parent */
    event(e, d) {
        this.dispatchEvent(new CustomEvent(e, { detail: d }));
    }
}
