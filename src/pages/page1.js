/** Core imports */
import { LitBase } from '../../deps/lit-base.js';
import { html }  from '../../deps/lit-html/lit-html.js';
import { styleMap } from '../../deps/lit-html/directives/style-map.js';

export class Page1 extends LitBase {
    static get observedProperties() {
        return ['appData', 'appServices'];
    }

    constructor() {
        super();
    }

    styles = {
        mainContainer: styleMap({
            padding      : '4px',
            display      : 'flex',
            height       : '100%',
            flexDirection: 'column',
            background   : 'lightgrey',
            gap          : '8px'
        })
    }

    render() {
        return html`
            <style>
                button {
                    height: 40px;
                    width: 100px
                }
            </style>
            <div style=${this.styles.mainContainer}>
                <span>Page 1</span>
                <span>Current Lang: ${this.appData.locale}</span>
                <button @click=${() => this.event('toggleLang')}>Toggle lang</button>
                <button @click=${() => this.appServices.showAlert('Hello there!!')}>Show Alert</button>
            </div>
        `
    }
}
customElements.define('my-page1', Page1);
